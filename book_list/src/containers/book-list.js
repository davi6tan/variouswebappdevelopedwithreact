import React, {Component} from 'react';
import {connect} from 'react-redux';
import {selectBook} from '../actions/index';
import {bindActionCreators} from 'redux';

class BookList extends Component {
    constructor(props) {
        super(props);
    }

    _renderList() {

        return this.props.books.map((book) => {

            return (

                <li
                    className="list-group-item"
                    onClick={() => this.props.selectBook(book)}
                    key={book.title}
                >
                    {book.title}
                </li>

            )

        });

    }

    ////////
    render() {

        return (

            <ul className="list-group col-sm-4">
                {this._renderList()}

            </ul>
        );
    }
}

/// statetoProps
function mapStateToProps(state) {
    return {books: state.books};
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({selectBook: selectBook}, dispatch);
}
//http://redux.js.org/docs/api/bindActionCreators.html
//http://redux.js.org/docs/recipes/ComputingDerivedData.html
export default connect(mapStateToProps, mapDispatchToProps)(BookList);
//export default connect(mapStateToProps)(BookList);