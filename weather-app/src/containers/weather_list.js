import React, {Component} from 'react';
import {connect} from 'react-redux';
import Chart from '../components/chart';
import GoogleMap from '../components/google_map';


class WeatherList extends Component {
    renderWeather(cityData) {
        const name = cityData.city.name;
        const temps = cityData.list.map(token => token.main.temp - 273.15);//temperature
        const pressures = cityData.list.map(token => token.main.pressure);
        const humidities = cityData.list.map(token => token.main.humidity);
        const {lon, lat} = cityData.city.coord;

        return (
            <tr key={name}>
                <td>{name}
                    <GoogleMap
                        lon={lon}
                        lat={lat}
                    />
                </td>
                <td>
                    <Chart
                        data={temps}
                        color="orange"
                        units="C"
                    />
                </td>
                <td>
                    <Chart
                        data={pressures}
                        color="green"
                        units="hPa"
                    />
                </td>
                <td>
                    <Chart
                        data={humidities}
                        color="black"
                        units="%"
                    />
                </td>
            </tr>
        )


    }

//http://v4-alpha.getbootstrap.com/content/tables/
    render() {
        console.log(this.props.weather)

        return (
            <table className="table table-hover">
                <thead>
                <tr>
                    <th>City</th>
                    <th>Temperature(C)</th>
                    <th>Pressure (hPA)</th>
                    <th>Humidity (%)</th>
                </tr>
                </thead>
                <tbody>
                {this.props.weather.map(this.renderWeather)}
                </tbody>
            </table>
        )
    }

}

function mapStateToProps({weather}) {
    return {weather};
}

export default connect(mapStateToProps)(WeatherList);