# A web app developed with React to search you tube

#### To run

      npm install

      npm start

      http://localhost:8080/
      

*Based on Stephen Grider's example*

### Landing page
      
![post](./diagrams/landing.png)

Tools used :

- [google api](https://console.developers.google.com)
      
- [media](http://getbootstrap.com/css/)      

- [react](https://facebook.github.io/react/)
      