# React App - Blog Posts




- [blog posts](https://react-es6-blog-posts.herokuapp.com/)


*Based on Stephen Grider's example*


### Tools Used:

- [redux](http://redux.js.org/)
- [react](https://facebook.github.io/react/)
- [react router](https://github.com/ReactTraining/react-router)
- [redux promise](https://www.npmjs.com/package/redux-promise)
- [redux form](https://www.npmjs.com/package/redux-form)