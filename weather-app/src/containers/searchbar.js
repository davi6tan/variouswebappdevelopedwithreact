import React, {Component} from 'react';
// redux connext, bindaction.. fetdch wearher
import {fetchWeather} from '../actions/index';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';


class SearchBar extends Component {
    constructor(props) {
        super(props);

        this.state = {term: ''};
        this._onFormSubmit = this._onFormSubmit.bind(this);
        this._onInputChange = this._onInputChange.bind(this);

    }

    _onInputChange(e) {
        this.setState({term: e.target.value});
    }

    _onFormSubmit(event) {
        /// prevent default - default for form is to reload the page when submit
        event.preventDefault();

        // We need to go and fetch weather data
        this.props.fetchWeather(this.state.term);
        this.setState({ term: '' });
    }

    ///
    render() {
//https://v4-alpha.getbootstrap.com/components/buttons/
        return (
            <form onSubmit={this._onFormSubmit} className="input-group">
                <input
                    placeholder="Get a five-day forecast in your favorite city"
                    className="form-control"
                    value={this.state.term}
                    onChange={this._onInputChange}
                />
                <span className="input-group-btn">
                    <button
                        type="submit"
                        className="btn btn-secondary"
                    >Submit</button>
                </span>
            </form>

        )
    }


}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({fetchWeather}, dispatch);
}
export default connect(null, mapDispatchToProps)(SearchBar);