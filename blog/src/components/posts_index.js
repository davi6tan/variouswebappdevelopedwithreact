import React, {Component} from 'react';
import {connect} from 'react-redux';
import {fetchPosts} from '../actions/index';
import {Link} from 'react-router';


class PostsIndex extends Component {
    /// about to be rendered 1st time
    componentWillMount() {
        //console.log("call action creator to fetch post");
        this.props.fetchPosts();
    }

    /// renderPosts
    renderPosts() {
        return this.props.posts.map((post) => {
            return (
                <li className="list-group-item row" key={post.id}>
                    <Link to={"posts/" + post.id}>
                        <div className="col-md-4">{post.categories}</div>
                        <div className="col-md-4"></div>
                        <div className="col-md-4"><b>{post.title}</b></div>
                    </Link>
                </li>
            );
        });
    }

    render() {
        return (
            <div className="row">
                <div className="add-post-btn col-md-12">
                    <Link to="/posts/new" className="btn btn-primary pull-right">Add a post</Link>
                </div>
                <div className="col-md-12 post-title"><h3>Posts</h3></div>
                <div className="col-md-12 post-listing">
                    <ul className="list-group">
                        {this.renderPosts()}
                    </ul>

                </div>

            </div>


        );
    }
}

function mapStateToProps(state) {
    return {posts: state.posts.all};
}

export default connect(mapStateToProps, {fetchPosts})(PostsIndex);

/*
 Object
 categories
 :
 "New Year's Day"
 id
 :
 50927
 title
 :
 "This is a new post"
 If using bind creator

 import {bindActionCreators} from 'redux';
 function mapDispatchToProps(dispatch) {
 return bindActionCreators({fetchPosts}, dispatch);
 }
 export default connect(null, mapDispatchToProps)(PostsIndex);
 // same as above
 export default connect(null, {fetchPosts})(PostsIndex);
 */