# React Redux example with a weather app

### To run

     npm install

     npm start

     http://localhost:8080/
     
     
#### Landing page
     
![post](./notes/final8080.png)


*Based on Stephen Grider's example*


### Tools Used:

- [redux](http://redux.js.org/)
- [react](https://facebook.github.io/react/)
- [react sparklines](https://www.npmjs.com/package/react-sparklines)
- [react google maps](https://www.npmjs.com/package/react-google-maps)

