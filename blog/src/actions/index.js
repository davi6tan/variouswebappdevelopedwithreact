import axios from 'axios';
export const FETCH_POSTS = 'FETCH_POSTS';
//create , delete
export const FETCH_POST = 'FETCH_POST';
export const CREATE_POST = 'CREATE_POST';

const ROOT_URL = "https://reduxblog.herokuapp.com/api";
const API_KEY = '?key=adogjumpoverthebluefence';
const COM_URL = `${ROOT_URL}/posts${API_KEY}`;
//const API_KEY = '';
//http://reduxblog.herokuapp.com/api/posts?key=adogjumpoverthebluefence
export function fetchPosts() {
    const request = axios.get(COM_URL);

    return {
        type: FETCH_POSTS,
        payload: request
    }


}
export function fetchPost(id) {
    const request = axios.get(`${ROOT_URL}/posts/${id}/${API_KEY}`)

    return {
        type: FETCH_POST,
        payload: request
    }


}
export function deletePost(id) {
    const request = axios.delete(`${ROOT_URL}/posts/${id}/${API_KEY}`)

    return {
        type: FETCH_POST,
        payload: request
    }


}

export function createPost(props) {
    const request = axios.post(COM_URL, props);
    return {
        type: CREATE_POST,
        payload: request
    }
}

