export function selectBook(book) {
    //console.log("action",book)

    return {
        type: 'BOOK_SELECTED',
        payload: book
    }
}