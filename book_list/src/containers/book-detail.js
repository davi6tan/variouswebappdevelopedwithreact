import React, {Component} from 'react';
import {connect} from 'react-redux';

/// mapStatetoProps

class BookDetail extends Component {
    constructor(props) {
        super(props);

    }

    render() {
        /// if empty
        if (!this.props.book) {
            return (
                <div className="book-detail col-sm-8">
                    Select a book to get started!
                </div>
            )
        }

        return (
            <div className="book-detail col-sm-8">
                <h3>Details for:</h3>
                <div>Title: {this.props.book.title}</div>
                <div>Pages: {this.props.book.pages}</div>
            </div>
        )

    }
}


function mapStateToProps(state) {
    return {book: state.activeBook}
}

export default connect(mapStateToProps)(BookDetail);