import axios from 'axios';
const API_KEY = '6a78596d062df78380eff5944c4e5567';
const ROOT_URL = `http://api.openweathermap.org/data/2.5/forecast?APPID=${API_KEY}`;
export const FETCH_WEATHER = 'FETCH_WEATHER';

export function fetchWeather(city) {
    const url = `${ROOT_URL}&q=${city},us`;
    const request = axios.get(url);
    // action always has type
    // axios behave like a jquery ajax
    return {
        type: FETCH_WEATHER,
        payload: request
    }

}

/*

 Notes
 http://openweathermap.org/forecast5
 api.openweathermap.org/data/2.5/forecast?q={city name},{country code}
 http://api.openweathermap.org/data/2.5/forecast/city?id=524901&APPID=1111111111
 api.openweathermap.org/data/2.5/forecast?q=London,us&mode=xml


 */