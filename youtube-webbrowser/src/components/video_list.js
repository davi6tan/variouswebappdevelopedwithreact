import React from 'react';
import VideoListItems from './video_list_item';


const VideoList = (props) => {
    const videos = props.videos;
    const videoItems = videos.map((video) => {
        return (

            <VideoListItems
                onVideoSelect = {props.onVideoSelect}
                key = {video.etag}
                video={video}
            />
        )
    })
    //console.log(videos)//[Object, Object, Object, Object, Object]

    return (
        <ul className="col-md-4 list-group">
            {videoItems}
        </ul>
    )
}

export default VideoList;